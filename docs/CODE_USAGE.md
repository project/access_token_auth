# How to use in code
The module provides the `Token Manager Factory` service, which is the main service used to load and manage token managers for access and token validation. Additionally, there are other related services available.

## Token Manager Factory
The `access_token_auth.token_manager_factory` service is responsible for loading the default or selected token manager service. The returned service implements the `TokenManagerInterface` interface.

### Token Manager Interface
The `TokenManagerInterface` interface defines the methods required to implement a custom `Token Manager`. This allows for flexibility and scalability in case the default token service does not meet specific requirements. By implementing a custom `Token Manager`, you can create, validate, and authenticate access tokens. The default `Token Manager` is already provided, but custom implementations can be used to fulfill specific use cases.

### Token List Interface
This interface introduces new methods to get all the available valid access tokens generated by an user. The main goal of this interface is to support the token list display. If you don't want to support it, you don't have to implement it.

## Default Token Manager
The default token manager service (`access_token_auth.default_token_manager`) is responsible for creating, validating, and authenticating access tokens. It associates access tokens with the user who generates them and stores them in the database for validation. In general, it is recommended to use the `Token Manager Factory` service instead of directly accessing the default token manager service in your code.

## Other services
- **Token Generator**: The token generator service (`access_token_auth.token_generator`) implements the `TokenGeneratorInterface` and is responsible for creating unique token values.

## Token Manager Service Tag
The `access_token_auth.token_manager` service tag allows you to discover and use custom `Token Manager` services in your own site.

See [How to Create Your Own Custom Token Manager](./CUSTOM_TOKEN_MANAGER.md).
