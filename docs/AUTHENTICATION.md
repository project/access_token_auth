# Authentication
To authenticate using the generated token, you have two options: `headers` or `parameters`.

Regardless of your choice, you only need to include the `X-ACCESS-AUTH-TOKEN` variable with the token value.

## Authenticate with access token
1. In your preferred client, pass the token value using the `X-ACCESS-AUTH-TOKEN` variable name.
   - You can pass in via headers or query parameters.
2. That's it!
