# Configuration
The module offers two types of settings for the generated access tokens: `Time-based` and `One-time use`.

If the user modifies the settings while tokens have already been generated, the existing tokens will adapt to the current settings. This means that regardless of the original configuration, the module automatically adjusts the tokens. For example, if a token was generated with the `Time-based` option and has already been used but remains valid within the time window, changing the settings to `One-time` will mark the token as used, as it was already used. The same behavior applies in the opposite scenario.

Additionally, you have the flexibility to decide whether users should be able to create multiple valid access tokens or have the system return an already existing valid token instead of generating a new one. This can be controlled using the `Create a single token` checkbox.

In the advanced settings, you can further customize the module's behavior by selecting a `Token Manager` and choosing whether to use a stub user for authentication when utilizing a generated token. More details on these options are provided below.

## Time-based
This is the default option, which sets a Time To Live (TTL) in seconds for the generated access tokens. The tokens remain valid for a specified period of time. By default, the TTL value is set to `180` seconds.

## One-time use
This option allows tokens to be used only once.

## Create a Single Token
By default, this option is disabled. When a user clicks the `Generate Token` button, a new token is generated each time. However, if enabled, the generation process will check for valid tokens; if any exist, it will return the most recent valid token without creating a new one. Otherwise, it will generate a new token.

## Token Manager
In this section, you can select from the available token managers. By default, the only token manager this module provides is the `Default Token Manager`. Further details on token managers are provided below.

## Use Stub User
By default, this option is disabled. When enabled, users can choose a stub user for authentication when utilizing a generated token.
