# Usage
Using the Access Token Auth module is straightforward. Follow these steps (for UI administration):

# Generate token
1. Enable the module.
2. Go to `/admin/config/services/access-token-auth`.
3. Configure the settings according to your preferences and save the changes.
4. Press the `Generate Token` button.

## Access available token list
1. Go to `/admin/config/services/access-token-auth/list`.
2. That's all! You'll be able to access all the valid access tokens the current user has generated.

## Get the value of a valid token
1. Go to `/admin/config/services/access-token-auth/list`.
2. On the actions column of the access token you want to get, click on the `Show token value` button.
3. That's it!

## Invalidate a token
1. Go to `/admin/config/services/access-token-auth/list`.
2. On the `Actions` column of the token you want to invalidate, press the `Invalidate token` button.
3. Press `Confirm`.
4. That's all folks!
