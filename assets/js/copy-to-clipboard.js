// eslint-disable-next-line no-unused-vars
function copyToClipboard(event) {
  event.preventDefault();
  const token = document.getElementById('token-raw-value').innerHTML;
  const tooltip = document.getElementById('tooltip');

  navigator.clipboard.writeText(token).then(() => {
    tooltip.classList.add('show');
    setTimeout(() => {
      tooltip.classList.remove('show');
    }, 1000);
  });
}
