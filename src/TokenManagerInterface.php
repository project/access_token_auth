<?php

namespace Drupal\access_token_auth;

use Drupal\access_token_auth\Model\Token;

/**
 * Interface for token manager services.
 */
interface TokenManagerInterface {

  /**
   * Gets the service readable name to be presented in the settings form.
   *
   * @return string
   *   The service readable name.
   */
  public function getReadableName(): string;

  /**
   * Generate a unique access token.
   *
   * @param int $user_id
   *   The user id of the user generating the token.
   *
   * @return \Drupal\access_token_auth\Model\Token|null
   *   The token object or NULL if there was an error while generating it.
   */
  public function generateToken(int $user_id): ?Token;

  /**
   * Check whether a token is valid or not.
   *
   * @param string $token
   *   The token value.
   *
   * @return \Drupal\access_token_auth\Model\Token|null
   *   The token object or NULL if not valid or found.
   */
  public function validateToken(string $token): ?Token;

  /**
   * Get a valid token by its value.
   *
   * @param string $token
   *   The token value.
   *
   * @return \Drupal\access_token_auth\Model\Token|null
   *   The token object or NULL if not found.
   */
  public function getToken(string $token): ?Token;

  /**
   * Get a valid token by its id.
   *
   * @param int $id
   *   The token id.
   *
   * @return \Drupal\access_token_auth\Model\Token|null
   *   The token object or NULL if not found.
   */
  public function getTokenById(int $id): ?Token;

  /**
   * Get a valid token from a specific user.
   *
   * @param int $user_id
   *   The user ID to get valid tokens from.
   *
   * @return \Drupal\access_token_auth\Model\Token|null
   *   The token object or NULL if not found.
   */
  public function getValidUserToken(int $user_id): ?Token;

  /**
   * Invalidate token once it's used.
   *
   * The existing tokens will adapt to the current settings mode, whether to
   * validate through usage or time. Pass the full_invalidate boolean flag to
   * completely invalidate a token, regardless of the authentication mode.
   *
   * @param string $token
   *   The token value.
   * @param bool $full_invalidate
   *   Whether to invalidate completely a token. A token is completely
   *   invalidated when the expiration timestamp and used flag are set with
   *   values that invalidate the token completely.
   */
  public function invalidateToken(string $token, bool $full_invalidate = FALSE): void;

  /**
   * Remove a token.
   *
   * @param string $token
   *   The token value.
   */
  public function removeByToken(string $token): void;

  /**
   * Remove tokens by user id.
   *
   * @param int $user_id
   *   The user id the tokens belong to.
   */
  public function removeByUser(int $user_id): void;

  /**
   * Remove expired tokens.
   *
   * @param int $timestamp
   *   The date used as reference to remove older tokens.
   */
  public function removeExpiredTokens(int $timestamp): void;

}
