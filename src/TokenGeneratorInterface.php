<?php

namespace Drupal\access_token_auth;

use Drupal\access_token_auth\Model\Token;

/**
 * Interface for token generator services.
 */
interface TokenGeneratorInterface {

  /**
   * Generates a unique access token.
   *
   * @param int $user_id
   *   The user id.
   *
   * @return \Drupal\access_token_auth\Model\Token|null
   *   The token object.
   *
   * @throws \Drupal\access_token_auth\Exception\TokenGeneratorException
   *   When there's an error generating the access token.
   */
  public function generateToken(int $user_id): ?Token;

}
