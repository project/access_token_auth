<?php

namespace Drupal\access_token_auth\Cache\RequestPolicy;

use Drupal\access_token_auth\Authentication\AccessTokenAuthProvider;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Do not serve a page from cache if X-ACCESS-AUTH-TOKEN token is applicable.
 *
 * This policy disallows caching of requests that use access_token_auth access
 * for security reasons. Otherwise responses for authenticated requests can get
 * into the page cache and could be delivered to unprivileged users.
 *
 * @internal
 */
class DisallowAccessTokenRequests implements RequestPolicyInterface {

  /**
   * {@inheritdoc}
   */
  public function check(Request $request) {
    if (!(empty($request->headers->get(AccessTokenAuthProvider::TOKEN)) && empty($request->query->get(AccessTokenAuthProvider::TOKEN)))) {
      return self::DENY;
    }
  }

}
