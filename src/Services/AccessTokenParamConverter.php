<?php

namespace Drupal\access_token_auth\Services;

use Drupal\access_token_auth\TokenManagerInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

/**
 * Converts parameters for upcasting token IDs to Token objects.
 *
 * @DCG
 * To use this converter specify parameter type in a relevant route as follows:
 * @code
 * access_token_auth.access-token_parameter_converter:
 *   path: example/{record}
 *   defaults:
 *     _controller: '\Drupal\access_token_auth\Controller\AccessTokenAuthController::build'
 *   requirements:
 *     _access: 'TRUE'
 *   options:
 *     parameters:
 *       record:
 *        type: access-token
 * @endcode
 *
 * Note that for entities you can make use of existing parameter converter
 * provided by Drupal core.
 * @see \Drupal\Core\ParamConverter\EntityConverter
 * @see \Drupal\access_token_auth\Model\Token
 */
class AccessTokenParamConverter implements ParamConverterInterface {

  /**
   * The token manager service.
   *
   * @var \Drupal\access_token_auth\TokenManagerInterface
   */
  protected TokenManagerInterface $tokenManager;

  /**
   * Constructs a new AccessTokenParamConverter.
   *
   * @param \Drupal\access_token_auth\TokenManagerInterface $token_manager
   *   The token manager service.
   */
  public function __construct(TokenManagerInterface $token_manager) {
    $this->tokenManager = $token_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults): mixed {
    return $this->tokenManager->getTokenById($value);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool {
    return !empty($definition['type']) && $definition['type'] == 'access-token';
  }

}
