<?php

namespace Drupal\access_token_auth\Services;

use Drupal\access_token_auth\Exception\TokenGeneratorException;
use Drupal\access_token_auth\Model\Token;
use Drupal\access_token_auth\TokenGeneratorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Generate unique tokens per user.
 */
class TokenGenerator implements TokenGeneratorInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * TokenGenerator class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function generateToken(int $user_id): Token {
    try {
      $token = hash('sha256', bin2hex(random_bytes(64)));
    }
    catch (\Throwable $exception) {
      throw new TokenGeneratorException($exception->getMessage());
    }

    if (empty($token)) {
      throw new TokenGeneratorException('No token was generated.');
    }

    $ttl = $this->configFactory->get('access_token_auth.settings')->get('ttl') ?: Token::DEFAULT_TTL;
    $expiration = time() + $ttl;

    return new Token($token, $user_id, $expiration);
  }

}
