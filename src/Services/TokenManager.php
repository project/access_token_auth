<?php

namespace Drupal\access_token_auth\Services;

use Drupal\access_token_auth\Exception\TokenGeneratorException;
use Drupal\access_token_auth\Model\Token;
use Drupal\access_token_auth\TokenGeneratorInterface;
use Drupal\access_token_auth\TokenListInterface;
use Drupal\access_token_auth\TokenManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Session\AccountInterface;
use Psr\Log\LoggerInterface;

/**
 * Manager service for tokens.
 */
class TokenManager implements TokenManagerInterface, TokenListInterface {

  /**
   * The default token manager service id.
   */
  const DEFAULT_SERVICE_ID = 'access_token_auth.default_token_manager';

  /**
   * The token generator service.
   *
   * @var \Drupal\access_token_auth\TokenGeneratorInterface
   */
  protected TokenGeneratorInterface $tokenGenerator;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   *   Database connection.
   */
  protected Connection $connection;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * TokenManager class constructor.
   *
   * @param \Drupal\access_token_auth\TokenGeneratorInterface $token_generator
   *   The token generator service.
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(
    TokenGeneratorInterface $token_generator,
    Connection $connection,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    LoggerInterface $logger
  ) {
    $this->tokenGenerator = $token_generator;
    $this->connection = $connection;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getReadableName(): string {
    return 'Default Token Manager';
  }

  /**
   * {@inheritdoc}
   */
  public function generateToken(int $user_id): ?Token {
    try {
      if (!$this->get('create_single_token') || !($token = $this->getValidUserToken($user_id))) {
        $token = $this->tokenGenerator->generateToken($user_id);
        $this->insert($token);
        $this->logger->info('A token has been generated for user: @user_id', ['@user_id' => $user_id]);
      }

      return $token;
    }
    catch (TokenGeneratorException $exception) {
      $this->logger->error(
        'There was an error when trying to generate a token for user @user_id. \n Message: @message', [
          '@user_id' => $user_id,
          '@message' => $exception->getMessage(),
        ]
      );
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function validateToken(string $token): ?Token {
    $token = $this->getToken($token);

    if (!$token) {
      return NULL;
    }

    // Invalidate token (i.e. mark it as used) regardless of auth mode.
    $this->invalidateToken($token->getToken());
    $this->logger->info('A token has been validated for user: @user_id', ['@user_id' => $token->getUserId()]);

    return $token;
  }

  /**
   * Insert token into database.
   *
   * @param \Drupal\access_token_auth\Model\Token $token
   *   The token object.
   *
   * @return int
   *   The id in database after insert.
   *
   * @throws \Exception
   *   Database exception.
   */
  protected function insert(Token $token): int {
    return (int) $this->connection
      ->insert(Token::TABLE_NAME)
      ->fields([
        'token' => $token->getToken(),
        'user_id' => $token->getUserId(),
        'expire_at' => $token->getExpireAt(),
      ])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getToken(string $token): ?Token {
    $condition = $this->get('mode') === TOKEN::DEFAULT_MODE ?
      ['expire_at', time(), '>'] : ['used_status', FALSE];

    $query = $this->connection
      ->select(Token::TABLE_NAME, 't')
      ->fields('t')
      ->condition('token', $token)
      ->condition(...$condition);

    $token_raw = $query->execute()->fetchObject();

    if (empty($token_raw)) {
      return NULL;
    }

    return $this->createTokenObjectFromStdClass($token_raw);
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenById(int $id): ?Token {
    $query = $this->connection
      ->select(Token::TABLE_NAME, 't')
      ->fields('t')
      ->condition('id', $id);

    if (!$this->currentUser->hasPermission('access any access_token_auth')) {
      $query->condition('user_id', $this->currentUser->id());
    }

    $token_raw = $query->execute()->fetchObject();

    if (empty($token_raw)) {
      return NULL;
    }

    return $this->createTokenObjectFromStdClass($token_raw);
  }

  /**
   * {@inheritdoc}
   */
  public function getValidUserToken(int $user_id): ?Token {
    $condition = $this->get('mode') === TOKEN::DEFAULT_MODE ?
      ['expire_at', time(), '>'] : ['used_status', FALSE];

    $query = $this->connection
      ->select(Token::TABLE_NAME, 't')
      ->fields('t')
      ->condition('user_id', $user_id)
      ->condition(...$condition);

    $token_raw = $query->execute()->fetchObject();

    if (!$token_raw) {
      return NULL;
    }

    return $this->createTokenObjectFromStdClass($token_raw);
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateToken(string $token, bool $full_invalidate = FALSE): void {
    $fields = [
      'used_status' => TRUE,
    ];

    if ($full_invalidate) {
      $fields['expire_at'] = time() - 100;
    }

    $this->connection->update(Token::TABLE_NAME)
      ->fields($fields)
      ->condition('token', $token)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function removeByToken(string $token): void {
    $this->connection->delete(Token::TABLE_NAME)
      ->condition('token', $token)
      ->execute();

    $this->logger->info('The token @token has been removed', ['@token' => $token]);
  }

  /**
   * {@inheritdoc}
   */
  public function removeByUser(int $user_id): void {
    $this->connection->delete(Token::TABLE_NAME)
      ->condition('user_id', $user_id)
      ->execute();

    $this->logger->info('All tokens for user ID @user_id have been removed', ['@user_id' => $user_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function removeExpiredTokens(int $timestamp): void {
    $condition = $this->get('mode') === TOKEN::DEFAULT_MODE ?
      ['expire_at', $timestamp, '<'] : ['used_status', TRUE];

    $this->connection->delete(Token::TABLE_NAME)
      ->condition(...$condition)
      ->execute();

    $this->logger->info('All expired tokens have been removed');
  }

  /**
   * {@inheritdoc}
   */
  public function limit(): int {
    return 10;
  }

  /**
   * {@inheritdoc}
   */
  public function getValidTokenList(int $user_id): array {
    $tokens = [];
    $condition = $this->get('mode') === TOKEN::DEFAULT_MODE ?
      ['expire_at', time(), '>'] : ['used_status', FALSE];

    /** @var \Drupal\Core\Database\Query\SelectInterface */
    $query = $this->connection
      ->select(Token::TABLE_NAME, 't')
      ->fields('t')
      ->condition('user_id', $user_id)
      ->condition(...$condition)
      ->extend(PagerSelectExtender::class)
      ->limit($this->limit());

    $results = $query->execute()->fetchAll();
    foreach ($results as $result) {
      $tokens[] = $this->createTokenObjectFromStdClass($result);
    }

    return $tokens;
  }

  /**
   * Gets the data from our configuration object.
   *
   * @param string $key
   *   A string that maps to a key within the configuration data.
   *
   * @return mixed
   *   The data that was requested.
   */
  protected function get(string $key): mixed {
    return $this->configFactory->get('access_token_auth.settings')->get($key);
  }

  /**
   * Utility function to create token objects from a \stdClass object.
   *
   * @param \stdClass $token_raw
   *   The raw token value returned by a query.
   *
   * @return \Drupal\access_token_auth\Model\Token
   *   The generated token.
   */
  protected function createTokenObjectFromStdClass(\stdClass $token_raw): Token {
    return new Token($token_raw->token, $token_raw->user_id, $token_raw->expire_at, $token_raw->used_status, $token_raw->id);
  }

}
