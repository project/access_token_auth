<?php

namespace Drupal\access_token_auth\Processor;

use Drupal\access_token_auth\Services\TokenManager;
use Drupal\access_token_auth\TokenManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Token manager processor.
 *
 * Holds an array of all registered token managers to use the chosen preferred
 * manager.
 */
class TokenManagerProcessor {

  /**
   * Holds the array of registered token managers.
   *
   * @var \Drupal\access_token_auth\TokenManagerInterface[]
   */
  protected array $tokenManagers;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * TokenManagerProcessor class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->tokenManagers = [];
    $this->configFactory = $config_factory;
  }

  /**
   * Registers token managers.
   *
   * @param \Drupal\access_token_auth\TokenManagerInterface $token_manager
   *   The token manager being registered.
   * @param string $id
   *   The service unique id.
   */
  public function addTokenManager(TokenManagerInterface $token_manager, string $id): void {
    // We don't need to manually check if the ID already exists in the
    // registered token managers because the ID corresponds to the service ID
    // itself.
    // Drupal automatically handles the validation of service IDs, so we let
    // Drupal do its magic.
    $this->tokenManagers[$id] = $token_manager;
  }

  /**
   * Gets a token manager by its ID.
   *
   * @param string $id
   *   The service ID.
   *
   * @return \Drupal\access_token_auth\TokenManagerInterface|null
   *   The token manager or NULL if not found.
   */
  public function getTokenManager(string $id): ?TokenManagerInterface {
    return $this->tokenManagers[$id] ?? NULL;
  }

  /**
   * Gets all the registered token managers.
   *
   * @return \Drupal\access_token_auth\TokenManagerInterface[]
   *   The available and registered token managers.
   */
  public function getAvailableTokenManagers(): array {
    return $this->tokenManagers;
  }

  /**
   * Gets the chosen token manager.
   *
   * @return \Drupal\access_token_auth\TokenManagerInterface
   *   The token manager implementation.
   */
  public function getChosenTokenManager(): TokenManagerInterface {
    // Fallback to the module's default token manager in case the chosen token
    // manager does not exist anymore.
    $id = $this->configFactory->get('access_token_auth.settings')->get('token_manager') ?? TokenManager::DEFAULT_SERVICE_ID;
    return $this->tokenManagers[$id] ?? $this->tokenManagers[TokenManager::DEFAULT_SERVICE_ID];
  }

}
