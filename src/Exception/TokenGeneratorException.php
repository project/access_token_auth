<?php

namespace Drupal\access_token_auth\Exception;

/**
 * Token generator exception.
 */
class TokenGeneratorException extends \Exception {

}
