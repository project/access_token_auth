<?php

namespace Drupal\access_token_auth\Form;

use Drupal\access_token_auth\Model\Token;
use Drupal\access_token_auth\TokenManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form to invalidate a token.
 */
final class InvalidateTokenConfirmForm extends ConfirmFormBase {

  /**
   * The token to be invalidated.
   *
   * @var \Drupal\access_token_auth\Model\Token|null
   */
  protected ?Token $token;

  /**
   * The token manager service.
   *
   * @var \Drupal\access_token_auth\TokenManagerInterface
   */
  protected TokenManagerInterface $tokenManager;

  /**
   * InvalidateTokenConfirmForm class constructor.
   *
   * @param \Drupal\access_token_auth\TokenManagerInterface $token_manager
   *   The token manager service.
   */
  public function __construct(TokenManagerInterface $token_manager) {
    $this->tokenManager = $token_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('access_token_auth.token_manager_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'access_token_auth_invalidate_token_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?Token $token = NULL) {
    $this->token = $token;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to invalidate this token (@token_secure)?', [
      '@token_secure' => $this->token->getSecureToken(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('access_token_auth.token_list');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->tokenManager->invalidateToken($this->token->getToken(), TRUE);

    $this->messenger()->addStatus($this->t('Token (@token_secure) invalidated!', [
      '@token_secure' => $this->token->getSecureToken(),
    ]));
    $form_state->setRedirectUrl(new Url('access_token_auth.token_list'));
  }

}
