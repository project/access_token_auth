<?php

namespace Drupal\access_token_auth\Form;

use Drupal\access_token_auth\Model\Token;
use Drupal\access_token_auth\Processor\TokenManagerProcessor;
use Drupal\access_token_auth\Services\TokenManager;
use Drupal\access_token_auth\TokenManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Psr\Container\ContainerInterface;

/**
 * Configure the token settings for this site.
 */
final class TokenSettingsForm extends ConfigFormBase {

  /**
   * The token manager service.
   *
   * @var \Drupal\access_token_auth\TokenManagerInterface
   */
  protected TokenManagerInterface $tokenManager;

  /**
   * The token manager processor.
   *
   * @var \Drupal\access_token_auth\Processor\TokenManagerProcessor
   */
  protected TokenManagerProcessor $tokenManagerProcessor;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * TokenSettingsForm class constructor.
   *
   * @param \Drupal\access_token_auth\TokenManagerInterface $token_manager
   *   The token manager service.
   * @param \Drupal\access_token_auth\Processor\TokenManagerProcessor $token_manager_processor
   *   The token manager processor.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    TokenManagerInterface $token_manager,
    TokenManagerProcessor $token_manager_processor,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->tokenManager = $token_manager;
    $this->tokenManagerProcessor = $token_manager_processor;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('access_token_auth.token_manager_factory'),
      $container->get('access_token_auth.token_manager_processor'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'access_token_auth_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['access_token_auth.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    if ($this->currentUser()->hasPermission('administer access_token_auth configuration')) {
      $this->buildConfigurationForm($form, $form_state);
    }

    $form['generate'] = [
      '#type' => 'details',
      '#title' => $this->t('Generate Token'),
      '#open' => TRUE,
    ];

    // To access this route, the current user must have at least the generate
    // token permission or administer config permission. If it only has the
    // 'administer config' permission, we infer the user can also generate
    // tokens.
    $form['generate']['generate_token'] = [
      '#type' => 'button',
      '#value' => $this->t('Generate Token'),
      '#ajax' => [
        'callback' => '::generateToken',
        'event' => 'click',
        'wrapper' => 'generated-token-wrapper',
      ],
    ];

    $form['generate']['token_output'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => ['generated-token-wrapper'],
      ],
    ];

    $form['#attached']['library'][] = 'access_token_auth/token-copy';

    return parent::buildForm($form, $form_state);
  }

  /**
   * Build the module configuration form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function buildConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#open' => TRUE,
    ];

    $form['general']['mode_options'] = [
      '#type' => 'radios',
      '#title' => $this->t('Authentication Mode'),
      '#description' => $this->t('Choose the token authentication mode.'),
      '#required' => TRUE,
      '#options' => [
        TOKEN::DEFAULT_MODE => $this->t('Time based'),
        'one_time' => $this->t('One time'),
      ],
      '#default_value' => $this->config('access_token_auth.settings')->get('mode') ?? TOKEN::DEFAULT_MODE,
    ];

    $form['general']['ttl'] = [
      '#type' => 'number',
      '#title' => $this->t('Token TTL'),
      '#description' => $this->t('The token time to live in seconds.'),
      '#default_value' => $this->config('access_token_auth.settings')->get('ttl') ?? Token::DEFAULT_TTL,
      '#min' => 30,
      '#max' => 1800,
      '#states' => [
        'visible' => [
          ':input[name="mode_options"]' => ['value' => TOKEN::DEFAULT_MODE],
        ],
      ],
    ];

    $form['general']['create_single_token'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create a single token'),
      '#description' => $this->t('Do not create a new token if a valid token is available.'),
      '#default_value' => $this->config('access_token_auth.settings')->get('create_single_token') ?? FALSE,
    ];

    $form['general']['delete_expired_tokens'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete expired tokens'),
      '#description' => $this->t('If selected, the tokens that have been used or whose timestamp has expired will be deleted in each cron run.'),
      '#default_value' => $this->config('access_token_auth.settings')->get('delete_expired_tokens') ?? FALSE,
    ];

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced Settings'),
    ];

    $token_manager_options = [];
    foreach ($this->tokenManagerProcessor->getAvailableTokenManagers() as $id => $token_manager) {
      $token_manager_options[$id] = $token_manager->getReadableName();
    }

    $default_value = $this->config('access_token_auth.settings')->get('token_manager') ?? TokenManager::DEFAULT_SERVICE_ID;
    $form['advanced']['token_manager_options'] = [
      '#type' => 'radios',
      '#title' => $this->t('Token Manager'),
      '#description' => $this->t('Choose the token manager you want the authentication mode to use.'),
      '#required' => TRUE,
      '#options' => $token_manager_options,
      '#default_value' => isset($token_manager_options[$default_value]) ? $default_value : TokenManager::DEFAULT_SERVICE_ID,
    ];

    $form['advanced']['use_stub'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use stub user'),
      '#description' => $this->t('When validating token, instead of authenticating as the user that generated the token, authenticate with a stub user.'),
      '#default_value' => $this->config('access_token_auth.settings')->get('use_stub') ?? FALSE,
    ];

    $stub_user = ($id = $this->config('access_token_auth.settings')->get('stub_user')) ? $this->entityTypeManager->getStorage('user')->load($id) : NULL;

    $form['advanced']['stub_user'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Stub user'),
      '#description' => $this->t('The stub user to load when authenticating a token.'),
      '#target_type' => 'user',
      '#default_value' => $stub_user,
      '#selection_handler' => 'default',
      '#states' => [
        'visible' => [
          ':input[name="use_stub"]' => ['checked' => TRUE],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('mode_options') === Token::DEFAULT_MODE && !$form_state->getValue('ttl')) {
      $form_state->setErrorByName('ttl', $this->t('You must set a value if the mode is time based.'));
    }

    if ($form_state->getValue('use_stub') && !$form_state->getValue('stub_user')) {
      $form_state->setErrorByName('stub_user', $this->t('You must choose a stub user.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('access_token_auth.settings')
      ->set('mode', $form_state->getValue('mode_options'))
      ->set('ttl', $form_state->getValue('ttl'))
      ->set('create_single_token', $form_state->getValue('create_single_token'))
      ->set('delete_expired_tokens', $form_state->getValue('delete_expired_tokens'))
      ->set('token_manager', $form_state->getValue('token_manager_options'))
      ->set('use_stub', $form_state->getValue('use_stub'))
      ->set('stub_user', $form_state->getValue('stub_user'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Generate a token ajax callback.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The altered form.
   */
  public function generateToken(array &$form, FormStateInterface $form_state): array {
    $token = $this->tokenManager->generateToken($this->currentUser()->id());
    $token = $token?->getToken() ?? $this->t('There was an error trying to generate the token');

    return $form['token_output'] = [
      '#theme' => 'token_clipboard',
      '#token' => $token,
    ];
  }

}
