<?php

namespace Drupal\access_token_auth\Form;

use Drupal\access_token_auth\TokenListInterface;
use Drupal\access_token_auth\TokenManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to display all the available tokens from the current user.
 */
final class TokenListForm extends FormBase {

  /**
   * The token manager service.
   *
   * @var \Drupal\access_token_auth\TokenManagerInterface
   */
  protected TokenManagerInterface $tokenManager;

  /**
   * TokenListForm class constructor.
   *
   * @param \Drupal\access_token_auth\TokenManagerInterface $token_manager
   *   The token manager service.
   */
  public function __construct(TokenManagerInterface $token_manager) {
    $this->tokenManager = $token_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('access_token_auth.token_manager_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'access_token_auth_token_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$this->tokenManager instanceof TokenListInterface) {
      $form['message'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('The current token manager (@token_manager_name) does not support token list management.', [
          '@token_manager_name' => $this->tokenManager->getReadableName(),
        ]),
      ];

      return $form;
    }

    $header = [
      'id' => $this->t('ID'),
      'token_value' => $this->t('Token'),
      'expiration_date' => $this->t('Expiration Date'),
      'used_status' => $this->t('Used Status'),
      'actions' => $this->t('Actions'),
    ];

    $form['table_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Token List'),
      '#description' => $this->t('List of the current valid tokens'),
    ];

    $form['table_container']['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t("There isn't any available valid token."),
    ];
    $this->buildRows($form['table_container']['table']);

    // Leverage and assume the query being used to retrieve the list of valid
    // tokens extends the \Drupal\Core\Database\Query\PagerSelectExtender so
    // that pagination is taken care of automatically.
    // If a custom token manager does not support the pager, it's advised to
    // alter this form to add its own pagination.
    $form['table_container']['pager'] = [
      '#type' => 'pager',
    ];

    return $form;
  }

  /**
   * Builds the rows for the table.
   *
   * @param array $table
   *   The table render element.
   */
  protected function buildRows(array &$table) {
    /** @var \Drupal\access_token_auth\Model\Token[] */
    $tokens = $this->tokenManager->getValidTokenList($this->currentUser()->id());
    foreach ($tokens as $token) {
      $table[$token->id()]['id'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $token->id(),
      ];

      $table[$token->id()]['token_value'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $token->getSecureToken(),
        '#attributes' => [
          'id' => ["protected-token-{$token->id()}-value"],
        ],
      ];

      $table[$token->id()]['expiration_date'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $token->getExpireAtDate()->format('Y-m-d H:i:s'),
      ];

      $table[$token->id()]['used_status'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $token->getUsedStatus() ? $this->t('Yes') : $this->t('No'),
      ];

      $table[$token->id()]['actions'] = [
        '#type' => 'container',
      ];

      $table[$token->id()]['actions']['show_token'] = [
        '#type' => 'button',
        '#value' => $this->t('Show token value'),
        '#name' => $token->id(),
        '#ajax' => [
          'callback' => '::showTokenValue',
          'event' => 'click',
          'wrapper' => "protected-token-{$token->id()}-value",
        ],
      ];

      $table[$token->id()]['actions']['invalidate'] = [
        '#title' => $this->t('Invalidate token'),
        '#type' => 'link',
        '#url' => Url::fromRoute('access_token_auth.invalidate_token', ['token' => $token->id()]),
        '#attributes' => [
          'class' => ['button'],
        ],
      ];
    }
  }

  /**
   * Show token value ajax callback.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function showTokenValue(array &$form, FormStateInterface $form_state) {
    $row = $form_state->getTriggeringElement();
    $token_id = $row['#name'];
    $token = $this->tokenManager->getTokenById($token_id);
    $form['table_container']['table'][$token_id]['token_value']['#value'] = $token->getToken();
    return $form['table_container']['table'][$token_id]['token_value'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
