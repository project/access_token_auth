<?php

namespace Drupal\access_token_auth\Model;

/**
 * Token model class.
 */
class Token {

  /**
   * The token database table name to register tokens.
   */
  const TABLE_NAME = 'access_token_auth';

  /**
   * The default time to live of a token in seconds (3 minutes).
   */
  const DEFAULT_TTL = 180;

  /**
   * The default token mode use.
   */
  const DEFAULT_MODE = 'time_based';

  /**
   * The record ID.
   *
   * @var int|null
   */
  protected ?int $id;

  /**
   * The token.
   *
   * @var string
   */
  protected string $token;

  /**
   * The user ID.
   *
   * @var int
   */
  protected int $userId;

  /**
   * The token's expiration date.
   *
   * @var int
   */
  protected int $expireAt;

  /**
   * Flag to indicate whether the token has been used or not.
   *
   * @var bool
   */
  protected bool $usedStatus;

  /**
   * Token class constructor.
   *
   * @param string $token
   *   The token.
   * @param int $user_id
   *   The user id.
   * @param int $expire_at
   *   The expiration date timestamp.
   * @param bool $used_status
   *   Whether the token has been used or not.
   * @param int $id
   *   The token record id.
   */
  public function __construct(string $token, int $user_id, int $expire_at, bool $used_status = FALSE, ?int $id = NULL) {
    $this->token = $token;
    $this->userId = $user_id;
    $this->expireAt = $expire_at;
    $this->usedStatus = $used_status;
    $this->id = $id;
  }

  /**
   * Get the id.
   *
   * @return int|null
   *   Return the token record id or NULL if it hasn't been set.
   */
  public function id(): ?int {
    return $this->id;
  }

  /**
   * Set the token record id.
   *
   * @param int $id
   *   The record id.
   */
  public function setId(int $id): void {
    $this->id = $id;
  }

  /**
   * Get the token.
   *
   * @return string
   *   Return the token value.
   */
  public function getToken(): string {
    return $this->token;
  }

  /**
   * Generate a secure token showing only the last four characters.
   *
   * @return string
   *   The secured token.
   */
  public function getSecureToken(): string {
    return 'xxxxxxxx' . substr($this->token, -4);
  }

  /**
   * Get the user id.
   *
   * @return int
   *   Return user id.
   */
  public function getUserId(): int {
    return $this->userId;
  }

  /**
   * Get the token's expiration date timestamp.
   *
   * @return int
   *   The expiration date timestamp
   */
  public function getExpireAt(): int {
    return $this->expireAt;
  }

  /**
   * Get the token's expiration date in a date time format.
   *
   * @return \DateTime
   *   The expiration date.
   */
  public function getExpireAtDate(): \DateTime {
    return (new \DateTime())->setTimestamp($this->expireAt);
  }

  /**
   * Get the token's used status.
   *
   * @return bool
   *   Flag to indicate whether the token is used or not.
   */
  public function getUsedStatus(): bool {
    return $this->usedStatus;
  }

}
