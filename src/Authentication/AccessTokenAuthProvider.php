<?php

namespace Drupal\access_token_auth\Authentication;

use Drupal\access_token_auth\TokenManagerInterface;
use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Access Token Auth authentication provider.
 */
class AccessTokenAuthProvider implements AuthenticationProviderInterface {

  const TOKEN = 'X-ACCESS-AUTH-TOKEN';

  /**
   * The token manager service.
   *
   * @var \Drupal\access_token_auth\TokenManagerInterface
   */
  protected TokenManagerInterface $tokenManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * AccessTokenAuthProvider class constructor.
   *
   * @param \Drupal\access_token_auth\TokenManagerInterface $token_manager
   *   The token manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(
    TokenManagerInterface $token_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory
  ) {
    $this->tokenManager = $token_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request): bool {
    return !(empty($request->headers->get(self::TOKEN)) && empty($request->query->get(self::TOKEN)));
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request): ?AccountInterface {
    $token = (string) $request->headers->get(self::TOKEN);
    if (empty($token)) {
      $token = (string) $request->query->get(self::TOKEN);
    }

    if (empty($token)) {
      return NULL;
    }

    $token = $this->tokenManager->validateToken($token);

    if (!$token) {
      return NULL;
    }

    $user_id = $token->getUserId();
    if ($this->configFactory->get('access_token_auth.settings')->get('use_stub')) {
      $user_id = $this->configFactory->get('access_token_auth.settings')->get('stub_user');
    }

    /** @var \Drupal\user\UserInterface|null */
    $user = $this->entityTypeManager->getStorage('user')->load($user_id);

    if (!$user || !$user->isActive()) {
      return NULL;
    }

    return $user;
  }

}
