<?php

namespace Drupal\access_token_auth;

/**
 * Interface for token manager that support token list management.
 */
interface TokenListInterface {

  /**
   * Specify the maximum number of elements per page for the token list.
   *
   * @return int
   *   An integer specifying the number of elements per page.
   */
  public function limit(): int;

  /**
   * A user's list of valid tokens.
   *
   * It's advised to use the \Drupal\Core\Database\Query\PagerSelectExtender to
   * support pagination in this query, as this is the function to be called on
   * the \Drupal\access_token_auth\Form\TokenListForm to build the table and
   * pagination of available tokens.
   *
   * @param int $user_id
   *   The user's id to retrieve tokens from.
   *
   * @return \Drupal\access_token_auth\Model\Token[]
   *   The list of valid tokens.
   *
   * @see \Drupal\Core\Database\Query\PagerSelectExtender
   */
  public function getValidTokenList(int $user_id): array;

}
