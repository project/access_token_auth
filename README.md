# Access Token Auth
The Access Token Auth module enables the generation of access tokens for authenticating requests within Drupal.

Other modules that this was based on:
- [REST API Access Token](https://www.drupal.org/project/rest_api_access_token)
- [Simple OAuth (OAuth2) & OpenID Connect](https://www.drupal.org/project/simple_oauth)

## Features
The Access Token Auth module focuses solely on the creation of access tokens, making it a straightforward solution. However, it's important to consider permissions because the generated token will authenticate requests as the user who generated the token, which is the currently logged-in user.

## Table of Contents
1. [Configuration](./docs/CONFIGURATION.md)
2. [Authentication](./docs/AUTHENTICATION.md)
3. [Usage](./docs/USAGE.md)
4. [Code Usage](./docs/CODE_USAGE.md)
5. [Create Custom Token Manager](./docs/CUSTOM_TOKEN_MANAGER.md)
